# -*- coding: utf-8 -*-
from configparser import ConfigParser


name = 'ID-Bank tool'
version = '0.8'

config = ConfigParser()
config.read('config.cfg')

# Справочник полей формы. Включает идентификатор поля, параметры, адреса соответствующих тегов во всех шаблонах xml
form_fields = [
    {
        'id': 'doccode', 'type': 'combobox', 'hidden': False, 'state': 'readonly',
        'values': ('O_IP_ACT_GACCOUNT_MONEY', 'O_IP_ACT_CURRENCY_ROUB', 'O_IP_ACT_ENDGACCOUNT_MONEY', 'O_IP_REQ_IP_BANKS'),
        'def_value': 'O_IP_ACT_GACCOUNT_MONEY',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/DocCode'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/DocCode'
            ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/DocCode'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/DocCode'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:DocType'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:DocType'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'fssp:DocType',
                'fssp:DetIPDocsInfo/fssp:IPDocsInfo/fssp:DocType'
            ],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'amount', 'type': 'entry', 'tag': 'Amount', 'hidden': False, 'def_value': '',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Amount',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/IDSum',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/IPDebt'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/IP/IDSum',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/IP/IPDebt'
            ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Amount',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/IDSum',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/IPDebt'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/Amount',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/IP/IDSum',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/IP/IPDebt'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'fssp:DetMvvDatumDocument/fssp:MvvDatumAvailabilityAcc/fssp:Summa',
                'fssp:DetIdPaymentPropertiesOIp/fssp:IdPaymentProperties/fssp:Amount',
                'fssp:ArbitraryXmlData/fssp:OIpExtension/fssp:LimitAmount'
        ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'fssp:DetMvvDatumDocument/fssp:MvvDatumAvailabilityAcc/fssp:Summa',
                'fssp:DetIdPaymentPropertiesOIp/fssp:IdPaymentProperties/fssp:Amount',
                'fssp:ArbitraryXmlData/fssp:OIpExtension/fssp:LimitAmount'
        ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'fssp:DetIdPaymentPropertiesOIp/fssp:IdPaymentProperties/fssp:Amount',
                'fssp:ArbitraryXmlData/fssp:OIpExtension/fssp:LimitAmount'
            ],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'debtortype', 'type': 'combobox', 'values': ('1', '2', '3', ''), 'def_value': '',
        'hidden': False, 'state': 'readonly',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorType',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/OspProperty/OspDetail/DebtorType'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/IP/DebtorType'
            ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorType',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/OspProperty/OspDetail/DebtorType'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/IP/DebtorType'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:DebtorType'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:DebtorType'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': ['fssp:DebtorType'],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'cnum', 'type': 'entry', 'hidden': False,
        'xml_tag': {}, 'xml_encr_tag': {}, 'def_value': ''
    },
    {
        'id': 'debtorname', 'type': 'entry', 'hidden': False, 'def_value': '',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorName'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/IP/DebtorName'
            ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorName'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/IP/DebtorName'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:DbtrName'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:DbtrName'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': ['fssp:DbtrName'],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'inn', 'type': 'entry', 'hidden': False, 'def_value': '',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorINN'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/IP/DebtorINN'
            ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorINN'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/IP/DebtorINN'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:DbtrInn'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:DbtrInn'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': ['fssp:DbtrInn'],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'birthdate', 'type': 'entry', 'hidden': False, 'def_value': '',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorBirthDate'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/IP/DebtorBirthDate',
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/Data/IdentificationData/birthDate'
            ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorBirthDate'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/IP/DebtorBirthDate'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:idDbtrBorn'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:idDbtrBorn'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': ['fssp:idDbtrBorn'],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'birthyear', 'type': 'entry', 'hidden': False, 'def_value': '',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorBirthYear'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/IP/DebtorBirthYear'
            ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorBirthYear'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/IP/DebtorBirthYear'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [],
            'O_IP_ACT_CURRENCY_ROUB': [],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'birthplace', 'type': 'entry', 'hidden': False, 'def_value': '',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorBirthPlace'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/IP/DebtorBirthPlace'
            ],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/IP/DebtorBirthPlace'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/IP/DebtorBirthPlace'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:idDbtrBornadr'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:idDbtrBornadr'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': ['fssp:idDbtrBornadr' ],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'iddoctype', 'type': 'entry', 'def_value': '', 'hidden': False,
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Data/IdentificationData/TypeDoc'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/Data/IdentificationData/TypeDoc'
            ],

            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Data/IdentificationData/TypeDoc'
            ],

            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/Data/IdentificationData/TypeDoc'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:TypeDocCode'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:TypeDocCode'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:TypeDocCode' ],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'iddocnumber', 'type': 'entry', 'hidden': False, 'def_value': '',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Data/IdentificationData/NumDoc'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/Data/IdentificationData/NumDoc'
            ],

            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Data/IdentificationData/NumDoc'
            ],

            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/Data/IdentificationData/NumDoc'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:NumDoc'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:NumDoc'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:NumDoc' ],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'iddocseries', 'type': 'entry', 'hidden': False, 'def_value': '',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Data/IdentificationData/SerDoc'
            ],
            'O_IP_REQ_IP_BANKS': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns1:Query/Data/IdentificationData/SerDoc'
            ],

            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Data/IdentificationData/SerDoc'
            ],

            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/Data/IdentificationData/SerDoc'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:SerDoc'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:SerDoc'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': ['fssp:DetMvvDatumDocument/fssp:MvvDatumIdentificator/fssp:SerDoc'],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'account', 'type': 'entry', 'hidden': False,
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Data/accountDatum/Acc'
            ],
            'O_IP_REQ_IP_BANKS': [],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/Data/accountDatum/Acc'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': []
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': ['fssp:DetMvvDatumDocument/fssp:MvvDatumAvailabilityAcc/fssp:Acc'],
            'O_IP_ACT_CURRENCY_ROUB': ['fssp:DetMvvDatumDocument/fssp:MvvDatumAvailabilityAcc/fssp:Acc'],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [],
            'O_IP_REQ_IP_BANKS': []
        },
        'def_value': ''
    },
    {
        'id': 'InternalKey', 'type': 'entry', 'hidden': False, 'def_value': '{random}',
        'state': 'normal',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/InternalKey'
            ],
            'O_IP_REQ_IP_BANKS': [],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/InternalKey'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/RestrDocId'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [],
            'O_IP_ACT_CURRENCY_ROUB': [],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'DocNum', 'type': 'entry', 'hidden': False, 'def_value': '{random}', 'state': 'normal',
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/DocNum'
            ],
            'O_IP_REQ_IP_BANKS': [],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/DocNum'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/RestrDocNumber'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [],
            'O_IP_ACT_CURRENCY_ROUB': [],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [],
            'O_IP_REQ_IP_BANKS': []
        }
    },
    {
        'id': 'DocDate', 'type': 'entry', 'hidden': False,
        'xml_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/DocDate'
            ],
            'O_IP_REQ_IP_BANKS': [],
            'O_IP_ACT_CURRENCY_ROUB': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/DocDate'
            ],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [
                'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/RestrDocDate'
            ]
        },
        'xml_encr_tag': {
            'O_IP_ACT_GACCOUNT_MONEY': [],
            'O_IP_ACT_CURRENCY_ROUB': [],
            'O_IP_ACT_ENDGACCOUNT_MONEY': [],
            'O_IP_REQ_IP_BANKS': []
        },
        'def_value': '2017-02-07'
    }
]

# Количество колонок полей на форме
form_fields_columns = 2

# Координаты бд ID-Bank и учетная запись
id_bank_db_creds = {
    'host': config['database']['host'],
    'port': int(config['database']['port']),
    'database': config['database']['database'],
    'user': config['database']['user'],
    'password': config['database']['password']
}

# Справочник шаблонов xml для разных типов документов.
# Содержит названия файлов, пространства имен, адрес тега для зашифрованной части
doc_type_xml = {
    'O_IP_ACT_GACCOUNT_MONEY': [
        config['xml']['o_ip_act_gaccount_money_template'], config['xml']['o_ip_act_gaccount_money_out'],
        {
            'S': 'http://schemas.xmlsoap.org/soap/envelope/',
            'dx': 'http://www.red-soft.biz/ncore/dx/1.1',
            'ns3': 'http://www.red-soft.biz/schemas/fssp/common/2011/0.5'
        },
        config['xml']['o_ip_act_gaccount_money_encrypted'],
        {'fssp': 'http://www.fssprus.ru/namespace/order/2015/1'},
        'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/ResolutionBase'
    ],
    'O_IP_ACT_CURRENCY_ROUB': [
        config['xml']['o_ip_act_currency_roub_template'], config['xml']['o_ip_act_currency_roub_out'],
        {
            'S': 'http://schemas.xmlsoap.org/soap/envelope/',
            'dx': 'http://www.red-soft.biz/ncore/dx/1.1',
            'ns3': 'http://www.red-soft.biz/schemas/fssp/common/2011/0.5'
        },
        config['xml']['o_ip_act_currency_roub_encrypted'],
        {'fssp': 'http://www.fssprus.ru/namespace/order/2015/1'},
        'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/ns3:Restrictn/ResolutionBase'
    ],
    'O_IP_ACT_ENDGACCOUNT_MONEY': [
        config['xml']['o_ip_act_endgaccount_money_template'], config['xml']['o_ip_act_endgaccount_money_out'],
        {
            'S': 'http://schemas.xmlsoap.org/soap/envelope/',
            'dx': 'http://www.red-soft.biz/ncore/dx/1.1',
            'xz': 'http://www.red-soft.biz/schemas/fssp/common/2011/0.5'
        },
        config['xml']['o_ip_act_endgaccount_money_encrypted'],
        {'fssp': 'http://www.fssprus.ru/namespace/order/2015/1'},
        'S:Body/dx:DXBox/dx:DXPack/dx:Envelopes/dx:Envelope/dx:Document/xz:Restrictn/ResolutionBase'
    ],
    'O_IP_REQ_IP_BANKS': [
        config['xml']['o_ip_req_ip_banks_template'], config['xml']['o_ip_req_ip_banks_out'],
        {
            'S': 'http://schemas.xmlsoap.org/soap/envelope/',
            'dx': 'http://www.red-soft.biz/ncore/dx/1.1',
            'ns1': 'http://www.red-soft.biz/schemas/fssp/common/2011/0.5'
        },
        '', {}, ''
    ]
}

# Строка sql запроса в БД Id-Bank для физ. лиц
sql_string_fiz = '' \
             'select first 1 ' \
             'FULL_NAME as DebtorName, ' \
             'INN, ' \
             'BIRTH_DATE as BirthDate, ' \
             'BIRTH_YEAR as BirthYear, ' \
             'BIRTH_PLACE as BirthPlace, ' \
             'DOC_TYPE_ID as IdDocType, ' \
             'DOC_NUM as IdDocNumber, ' \
             'DOC_SER as IdDocSeries ' \
             "from bank_account_fiz BANK JOIN bank_account ACC on BANK.id = ACC.id " \
             "where ACC.ADDITIONAL_FIELD_1 = '{cnum}'"

# Строка sql запроса в БД Id-Bank для юр. лиц
sql_string_yur = '' \
             'select first 1 ' \
             'ORGANIZATION_NAME as DebtorName, ' \
             'INN ' \
             "from bank_account_yur BANK JOIN bank_account ACC on BANK.id = ACC.id " \
             "where ACC.ADDITIONAL_FIELD_1 = '{cnum}'"

sql_dup_rows_number = config['sql']['number_of_rows_to_return']

sql_string_dup_fiz_dict = {
    'basic_stmnt': 'select first {row_qnty} {t1_alias}.CNUM{cnum_2} as CNUM, {out_fields} from {table_1} {t1_alias}{join}',
    't1_alias': 'ip',
    't2_alias': 'fl',
    't3_alias': 'ul',
    'table_1': "(select list(ba.ADDITIONAL_FIELD_1, ';') as CNUM, {out_fields} from bank_account_fiz baz join bank_account ba on baz.id = ba.id "
               "where ba.account_type = 'ИП' group by {out_fields} having count({having_cond}) = {number_ip})",
    'table_2': "(select list(ba.ADDITIONAL_FIELD_1, ';') as CNUM, {out_fields} from bank_account_fiz baz join bank_account ba on baz.id = ba.id "
               "where ba.account_type = 'ФЛ' group by {out_fields} having count({having_cond}) = {number_fl})",
    'table_3': "(select list(ba.ADDITIONAL_FIELD_1, ';') as CNUM, {out_fields} from bank_account_yur bay join bank_account ba on bay.id = ba.id "
               "group by {out_fields} having count({having_cond}) = {number_ul})",
    'join_cond': '({t1_alias}.{field} = {t2_alias}.{field} or {t1_alias}.{field} is null or {t2_alias}.{field} is null)',
    'join_stmnt': ' join {table_2} {t2_alias} on {conditions}'
}
#({t1_alias}.{field} = {t2_alias}.{field} or {t1_alias}.{field} is null or {t2_alias}.{field} is null)
#{t1_alias}.{field} = {t2_alias}.{field}

sql_fileds_dict = {
    'debtorname': 'FULL_NAME',
    'inn': 'INN',
    'birthdate': 'BIRTH_DATE',
    'birthyear': 'BIRTH_YEAR',
    'birthplace': 'BIRTH_PLACE',
    'iddoctype': 'DOC_TYPE_ID',
    'iddocnumber': 'DOC_NUM',
    'iddocseries': 'DOC_SER'
}

get_acc_resp_name_space = {
    'out11': 'http://www.raiffeisen.ru/types/BankProduct/v1',
    'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
    'out13': 'http://www.raiffeisen.ru/services/ProductService/v1/schemas',
    'out25': 'http://www.raiffeisen.ru/types/CallStatus/v1',
    'io2': 'http://www.raiffeisen.ru/types/ProductId/v1',
    'out3': 'http://www.raiffeisen.ru/types/FmaId/v1'
}
get_acc_request = config['get_accounts']['request']
get_acc_endpoint = config['get_accounts']['endpoint']
